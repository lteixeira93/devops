import math
import os
import random
import re
import sys
import statistics

# Mean index 0
# Mean next indexes
# Store abs in list
# Display minimum

def locateEarliestMonth(stockPrice):
    current_average, next_average = (0, 0)
    net_prices = []
    
    for idx, _ in enumerate(stockPrice):
        # Break execution when average of the last is computed
        if len(stockPrice[idx+1:len(stockPrice)+1]) == 0:
            break
        
        # Get current and next averages
        current_average = int(statistics.mean(stockPrice[0:idx+1]))
        current_average = int(sum(stockPrice[0:idx+1])/len(stockPrice[0:idx+1]))
        next_average = int(statistics.mean(stockPrice[idx+1:len(stockPrice)+1]))
        next_average = int(sum(stockPrice[idx+1:len(stockPrice)+1])/len(stockPrice[idx+1:len(stockPrice)+1]))
        
        # Append mod net price
        net_prices.append(abs(current_average - next_average))
    
    minimum = min(net_prices)
    for idx, net_price in enumerate(net_prices):
        if net_price == minimum:
            return(idx + 1)
            break
    
if __name__ == '__main__':
    stockPrice = [1, 3, 2, 4, 5]

    print(locateEarliestMonth(stockPrice))
