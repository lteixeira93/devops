#!/bin/python3
import math
import os
import random
import re
import sys

def processLogs(logs, threshold):
    counter_dict = {}
    ref_dict = {}
    
    for log in logs:
        for idx, _ in enumerate(log.split()):
            # Skip last element
            if idx < 2:
                counter_dict[log.split()[idx]] = 0
    
    for log in logs:        
        for idx, count in enumerate(log.split()):
            if idx == 2:
                break
            if count in counter_dict:
                counter_dict[count] += 1
                if log.split()[0] == log.split()[1]:
                    break
        
    transaction_list = []
    for k, v in counter_dict.items():
        if v >= threshold:
            transaction_list.append(k)
            
    print(transaction_list)
    

if __name__ == '__main__':
    threshold = 3
    logs = ['1 3 40', '1 2 50', '1 7 70', '1 2 40']

    result = processLogs(logs, threshold)
    