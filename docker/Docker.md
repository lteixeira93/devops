# Notes

## Containers
```
Isolated linux environment with shared OS.
                API
                ---
            Docker Engine
                ---
                SO
```

## Docker VS VM
```
Docker:
    API
    ---
Docker Engine
    ---
    SO
    ---
   Infra

VM:
    API
    ---
  GUEST OS
    ---
 Hypervisor
    ---
    SO
    ---
   Infra
```

## Docker Filesystem
```
Layered
Docker uses busybox to emulate rootfs
For each instalation on container, a new layer is created
For instance, install debian, then emacs and the apache, at the end it will have 3 layers
Images are shared (Example, 2 containers using debian will share same debian image)
``` 

## Docker main commands
```
docker container --help
docker ps --help
docker image --help
docker exec -it CONTAINER_NAME sh or command (e.g. ifconfig)
docker run --name "web_server" -d -p 8080:80 -e NGINX_ENTRYPOINT_QUIET_LOGS=1 -v "/home/ltx/docker_fs:/usr/share/nginx/html" nginx:1.19.4-alpine
docker inspect
docker network
docker network create - Create your range of IPs and driver (Bridge, host)
docker run -d -p <host_port>:<container_port> --name
docker build -t <image_name> <path>
``` 

## Docker Volume
```
By default, data written in the container is not persisted.
Volume bypass this default behavior by storing files into host
``` 

## Docker Network
```
Docker creates internal network and assign IP to the containers.
``` 